import {mapState,mapGetters} from 'vuex'
export default {
  computed: {
    ...mapState('m_user',['userInfo']),
    ...mapGetters('m_user',['total']),
    // total: function() {
      
    //   return !this.userInfo?0:this.userInfo.myMessage + this.userInfo.myInform
    //   // return 0
    // }
  },
  onShow() {
    this.setBadge()
    console.log('执行onshow~')
  },
  methods : {
    setBadge() {
      if(this.total!=0){
        uni.setTabBarBadge({
          index: 2,
          text: this.total +''
        })
      }else {
       uni.removeTabBarBadge({
         index:2
       })
      }
    }
  },
  watch : {
    total : {
      handler(newVal){
        this.setBadge()
      },
      immediate: true
    } 
  }
}