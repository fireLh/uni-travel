import vue from 'vue'
import vuex from 'vuex'
import modulesUser from '@/store/user.js'
vue.use(vuex)

const store = new vuex.Store({
  modules : {
    'm_user': modulesUser
  }
})
export default store
