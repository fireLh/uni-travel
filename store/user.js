export default {
  //开启命名空间
  namespaced: true,
  //数据
  state: () => ({
    userInfo:JSON.parse(uni.getStorageSync('userInfo') || null),
  }),
  mutations: {
    updateUserInfo(state,info){
      state.userInfo = info
      this.commit('m_user/saveUserInfo')
    },
    updateUserInfoX(state,info){
    },
    saveUserInfo(state) {
      uni.setStorageSync('userInfo',JSON.stringify(state.userInfo))
    },
    delUserInfo(state) { 
      state.userInfo = null
      uni.removeStorageSync('userInfo')
     // GVinfo.DataSource = null;
     // GVinfo.DataBind();
      // this.$forceUpdate();//在方法最后面添加强制渲染
      // uni.redirectTo({
      //     //保留当前页面，跳转到应用内的某个页面
      //     url: '/subpkg/login/login'
      // })
    }
  },
    
  getters: {
    total(state) {
      if(!state.userInfo) return 0
      return state.userInfo.myMessage + state.userInfo.myInform
      // return !state.userInfo? 0 : state.userInfo.myMessage + state.userInfo.myInform
    }
  },
  
}